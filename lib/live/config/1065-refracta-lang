#!/bin/sh

## 1065-refracta-lang - Additional Configuration Script for Debian Live
## 
## script goes in /lib/live/config/ (or other alternative, see <man live-config>)
## best installed as a deb package
## 
## originally adapted from template in/usr/share/doc/live-config/examples
## automatically select language and keyboard from (boot prompt) cmdline
## 
## usage examples: components=refracta-lang lang=de_DE || components=refracta-lang lang=en_GB
## where both parts of a locale are the same, e.g. de_DE fr_FR it should work with just lang=de
## 
## There is no need to additionally use "components=locales locales=whatever"
## 
## There is no need to additionally use "components=keyboard-configuration keyboard-layouts=whatever"
## UNLESS a keyboard different from the locale is used
## 
## For Debian/Devuan Jessie
## This script should run after user-setup, after locales and before login
##
## Copyright (C) 2013 David Hare <dzz@exegnulinux.net>
## Copyright (C) 2006-2014 Daniel Baumann <mail@daniel-baumann.ch>
##
## 1065-refracta-lang comes with ABSOLUTELY NO WARRANTY; for details see COPYING.
## This is free software, and you are welcome to redistribute it
## under certain conditions; see COPYING for details.


Init ()
{
	# check if this script is already configured
	if [ -e /var/lib/live/config/refracta-lang ]
	then
		exit 0
	fi

	# if "lang=" was  not specified, stop now
	if [ "$(cat /proc/cmdline|grep -o "lang=.*")" = "" ]; then
		exit 0
	fi

	# if "locales=" was specified, stop now, use that instead
	if ! [ "$(cat /proc/cmdline|grep -o "locales=.*")" = "" ]; then
		exit 0
	fi

echo -n " refracta-lang "
	
}


get_configs ()
{

# see what "lang=" was specified
LANGSELECTED="$(cat /proc/cmdline|grep -o "lang=.*" | sed "s/ .*//" | sed "s/.UTF-8//"| sed "s/lang=//")"

# check now if selection is supported # else fallback if not to US
if [ "$(cat /usr/share/i18n/SUPPORTED|grep UTF-8|grep $LANGSELECTED|awk '{print $1}')" = "" ]; then
echo " locale $LANGSELECTED unsupported (or input error)"
LANGSELECTED="en_US"
fi

LOCALESELECTED="$LANGSELECTED.UTF-8"

# is keyboard already set by keyboard-configuration??
if ! [ "$(cat /proc/cmdline|grep -o "keyboard-layouts=.*")" = "" ]; then

# it was... use that
LAYOUTS="$(cat /proc/cmdline|grep -o "keyboard-layouts=.*" | sed "s/ .*//" | sed "s/keyboard-layouts=//")"

else
# get it from lang=something (was set or we would not be here)
# should be lang=de_DE, but if only "lang=de" was specified, done like this it still works!
LAYOUTS="$(echo $LANGSELECTED|sed s/[a-z][a-z]_// | sed s/'.UTF-8'// |tr '[A-Z][A-Z]' '[a-z][a-z]')"

fi

#get user id .. (was already set up by 002-user-setup and skel copied over)
NORMALUSER=$(cat /etc/passwd | grep 1000 |cut -d: -f1)

## files in xfce to deal with: ##

# might not exist
if [ -f /home/$NORMALUSER/.config/xfce4/panel/panels.xml ]; then
XKBPLUGINID=$(cat /home/$NORMALUSER/.config/xfce4/panel/panels.xml|grep xkbplugin |sed "s/<item name=\"xkbplugin\" id=\"//"|sed "s/\"\/>//")
XKBPLUGINFILE=/home/$NORMALUSER/.config/xfce4/panel/xkb-plugin-$XKBPLUGINID
fi

}

configure_locales ()
{

# this is (mostly unmodified) from (official) 004-locales
# these vars are already set by this script
#_LOCALE=$LOCALESELECTED
LIVE_LOCALES=$LOCALESELECTED

	if [ -e /etc/default/locale ]
	then
		# use rootfs configured locale
		_LOCALE=$(grep -s 'LANG=' /etc/default/locale | sed s/'LANG='// | tr -d '"' )
	fi

# use $LIVE_LOCALES if set
	if [ -n "${LIVE_LOCALES}" ]
	then
		_LOCALE="${LIVE_LOCALES}"
		_SET_LOCALE="true"
	fi

# in case it's zero
	if [ -z "${_LOCALE}" ]
	then
		# Set a default one
		_LOCALE="en_US.UTF-8"
		_SET_LOCALE="true"
	fi

# what is this $locale ? should it not be $_LOCALE ?
	_LANGUAGE="$(echo $_LOCALE | cut -d. -f1)"
	export _LANGUAGE

# stop now if $_SET_LOCALE  is other than "true"
	if [ -z "${_SET_LOCALE}" ]
	then
		# Actually, we should check here if the locale is generated
		return
	fi

# if $_LOCALE was set to e.g. "es" make it es_ES.UTF-8
	if echo "${_LOCALE}" | grep -sqE '^[[:lower:]]{2}$'
	then
		# input is like "locale=ch", so we will convert and setup also the keyboard if not already set
		if [ -z "${_KEYBOARD}" ]
		then
			_KEYBOARD="${_LOCALE}"
			export _KEYBOARD
		fi

		_LOCALE_UP=$(echo "${_LOCALE}" | tr '[a-z]' '[A-Z]')
		_LOCALE="${_LOCALE}_${_LOCALE_UP}.UTF-8"
	fi

	LANG=

# take off .UTF-8
	_LANGUAGE="$(echo ${_LOCALE} | cut -d. -f1)"

# check if is supported !!! I can't get this to work at all
	eval $(awk '/^'"${_LOCALE}"'/ { print "LANG=" $1 " codepage=" $2; exit; }' /usr/share/i18n/SUPPORTED)

	if [ -z "${LANG}" ]
	then
		# Try and fallback to another codepage for this language.
		eval $(awk '/^'"${_LANGUAGE}"'/ { print "LANG=" $1 " codepage=" $2; exit; }' /usr/share/i18n/SUPPORTED)

		if [ -n "${LANG}" ]
		then
			echo "Choosing locale '${LANG}' as '${_LOCALE}' is unsupported."
		fi
	fi

# if it's still unset fallback to US
	if [ -z "${LANG}" ]
	then
		echo "Locale '${_LOCALE}' is unsupported."
		_CODEPAGE="UTF-8"
		_LANGUAGE="en_US"
		_LOCALE="${_LANGUAGE}.${_CODEPAGE}"
		LANG="${_LANGUAGE}.${_CODEPAGE}"
	fi

	printf 'LANG="%s"\n' "${LANG}" > /etc/default/locale
	sed -i -e "s|# ${LANG} ${_CODEPAGE}|${LANG} ${_CODEPAGE}|" /etc/locale.gen

	locale-gen --keep-existing > /dev/null 2>&1

}

configure_keyboard ()
{

# there might be more than one xkb-plugin-* if it's a snapshot

# TODO to make this portable beyond refracta we need to write these files new (can't edit what may not exist)

# this might work instead
# sed -i "s/layouts=.*/layouts=$LAYOUTS/" $XKBPLUGINFILE
#for i in $( ls /home/$NORMALUSER/.config/xfce4/panel|grep xkb-plugin ); do
#sed -i "s/layouts=.*/layouts=$LAYOUTS/" /home/$NORMALUSER/.config/xfce4/panel/$i;
#done

# do we want to delete it or write a new one?
rm -f home/$NORMALUSER/.config/xfce4/xfconf/xfce-perchannel-xml/keyboard-layout.xml

	#  pick some preset alts from a "most-common" shortlist, e.g. "us,gb,de,fr,es,nl,pt,it" 
	#  remove user choic(es) from alt shortlist, remove any leading or trailing commas
	KB_ALT=$(echo "us,gb,de,fr,es,nl,pt,it" |sed "s/$LAYOUTS,//"|sed "s/,$LAYOUTS//")
	#  set user choice(es) first then alts, limit it all to 3 for xfce switcher
	XLAYOUTS="$(echo $LAYOUTS,$KB_ALT|awk -F , '{print $1","$2","$3}')"
	
	# this might work instead
	# sed -i "s/layouts=.*/layouts=$XLAYOUTS/" $XKBPLUGINFILE
	for i in $( ls /home/$NORMALUSER/.config/xfce4/panel|grep xkb-plugin ); do
	sed -i "s/layouts=.*/layouts=$XLAYOUTS/" /home/$NORMALUSER/.config/xfce4/panel/$i;
	done

	# do we want to delete it or write a new one?
	rm -f home/$NORMALUSER/.config/xfce4/xfconf/xfce-perchannel-xml/keyboard-layout.xml

##################################################################################################

# set the console keyboard
	
# if keyboard not already set by keyboard-configuration
if [ "$(cat /proc/cmdline|grep -o "keyboard-layouts=.*")" = "" ]; then

	# set it from "lang="
	# this will only support simple 2-letter ones (e.g. "es,fr") and not variants etc

	# lifted modded and simplified from official "keyboard-configuration" script
	sed -i -e "s|^XKBLAYOUT=.*$|XKBLAYOUT=\"$LAYOUTS\"|" /etc/default/keyboard
	echo "keyboard-configuration keyboard-configuration/layoutcode select $LAYOUTS" >> /tmp/debconf.live

		if [ -e /tmp/debconf.live ]
		then
		debconf-set-selections < /tmp/debconf.live
		rm -f /tmp/debconf.live
		fi

	# now, if this all works, an example DE user can do at cmdline:
	# live-config=refracta-lang lang=de_DE
	# we should be automatically configured with default keyboard (X and console) gdm and xfce session all in German
# else do nothing, i.e. use what was set by keyboard-configuration or default ("us")
fi	

#################################################################################################

# make sure root doesn't own user files
chown -R $NORMALUSER:$NORMALUSER /home/$NORMALUSER/.config/xfce4

# do dmrc # maybe no longer necessary
cat >/home/$NORMALUSER/.dmrc <<EOF

[Desktop]
Session=default

EOF
chown $NORMALUSER:$NORMALUSER /home/$NORMALUSER/.dmrc

}

# run each function
Init
get_configs
configure_locales
configure_keyboard

# Creating state file
touch /var/lib/live/config/refracta-lang
